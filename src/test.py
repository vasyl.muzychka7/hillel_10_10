from haversine import haversine, Unit

cities = {
    "Вінниця": (49 + 14/60, 28 + 30/60),
    "Івано-Франківськ": (48 + 56/60, 24 + 45/60),
    "Луцьк": (50 + 45/60, 25 + 15/60),
    "Львів": (49 + 51/60, 24 + 2/60),
    "Одеса": (46 + 28/60, 30 + 45/60),
    "Рівне": (50 + 35/60, 26 + 0/60),
    "Тернопіль": (49 + 34/60, 25 + 30/60),
    "Харків": (50, 36 + 13/60),
    "Херсон": (46 + 38/60, 32 + 30/60),
    "Чернівці": (48 + 17/60, 25 + 57/60)
}

def calculate_distance(city1, city2):
    return haversine(cities[city1], cities[city2], unit=Unit.KILOMETERS)

def print_distance(city1, city2, distance):
    print(f"Відстань між {city1} та {city2}: {distance:.2f} км")

def calculate_and_print_distances():
    for city1 in cities:
        for city2 in cities:
            if city1 != city2:
                distance = calculate_distance(city1, city2)
                print_distance(city1, city2, distance)

calculate_and_print_distances()




