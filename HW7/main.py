from route import Route
from bus import Bus
from ticket import Ticket


route1 = Route(1, "Start Location 1", "End Location 1")
bus1 = Bus(101, 50, route1)
ticket1 = Ticket("John Doe", bus1, 20)

# Виведення інформації
print(route1)
print(bus1)
print(ticket1)
