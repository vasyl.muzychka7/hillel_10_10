class Route:
    def __init__(self, number, start_location, end_location):
        self.number = number
        self.start_location = start_location
        self.end_location = end_location

    def __str__(self):
        return f"Route {self.number}: {self.start_location} to {self.end_location}"
