from route import Route
from bus import Bus
from ticket import Ticket
from controller import Controller

""""
route1 = Route(1, "Start Location 1", "End Location 1")
bus1 = Bus(101, 50, route1)
ticket1 = Ticket("John Doe", bus1, 20)

# Виведення інформації
print(route1)
print(bus1)
print(ticket1)
"""""
controller = Controller()


controller.display_routes()
controller.display_routes_to_city("End Location 1")
controller.display_tickets_for_bus_on_route(101, 1)