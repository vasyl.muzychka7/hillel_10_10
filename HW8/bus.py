from route import Route

class Bus:
    def __init__(self, number, capacity, route):
        self.number = number
        self.capacity = capacity
        self.route = route

    def __str__(self):
        return f"Bus {self.number}, Capacity: {self.capacity}, Route: {self.route}"
