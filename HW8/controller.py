from route import Route
from bus import Bus
from ticket import Ticket

class Controller:
    def __init__(self):
        self.routes = []
        self.buses = []
        self.tickets = []

    def add_route(self, route):
        self.routes.append(route)

    def add_bus(self, bus):
        self.buses.append(bus)

    def add_ticket(self, ticket):
        self.tickets.append(ticket)

    def display_routes(self):
        print("List of Routes:")
        for route in self.routes:
            print(route)

    def display_routes_to_city(self, city):
        print(f"List of Routes to {city}:")
        matching_routes = [route for route in self.routes if city.lower() in route.end_location.lower()]
        for route in matching_routes:
            print(route)

    def display_tickets_for_bus_on_route(self, bus_number, route_number):
        print(f"List of Tickets for Bus {bus_number} on Route {route_number}:")
        matching_tickets = [ticket for ticket in self.tickets if ticket.bus.number == bus_number and ticket.bus.route.number == route_number]
        for ticket in matching_tickets:
            print(ticket)
