from bus import Bus

class Ticket:
    def __init__(self, passenger_name, bus, price):
        self.passenger_name = passenger_name
        self.bus = bus
        self.price = price

    def __str__(self):
        return f"Ticket for {self.passenger_name}, Bus: {self.bus.number}, Price: {self.price}"
